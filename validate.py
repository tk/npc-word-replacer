import json
import sys

try:
    json.load(open('FoxReplace.json'))
except:
    print('Validation failed!')
    sys.exit(1)
else:
    print('Validation succeded')
    sys.exit(0)
