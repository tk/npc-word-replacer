Non-PC Word Replacer
--------------------
![Build status icon](https://travis-ci.org/Karunamon/npc-word-replacer.svg)

TNPWR, as I'll call it from here on, is a list of rules for
[FoxReplace](https://addons.mozilla.org/en-US/firefox/addon/foxreplace/), a
Firefox browser plugin that replaces words on websites.

This list provides a somewhat cynical view of the web, replacing known buzzwords
with more truthful (and, not gonna lie: opinionated) alternatives that I feel
either reflect reality more (and shake off the loaded meanings of some of those
words), or make politically charged topics a lot more funny to read.


Installing
----------
1. Install FoxReplace from the Firefox plugins site.
2. Open the plugin options, and toggle the "periodically update from URL" box.
3. Get the raw link to master repo copy of FoxReplace.json. It should be:
https://github.com/Karunamon/npc-word-replacer/raw/master/FoxReplace.json
4. Set the update options to as often as you're comfortable with.
5. Enjoy a more humorous, less loaded web browsing experience.

![FoxReplace options dialog](http://i.imgur.com/StEH2mz.png)

Usage
-----
Simply turn the plugin on after setting it up. I tried to separate the lists
by topic, so if you don't like one list, just toggle it off in the options.


Pull Request Rules
------------------
Contributions are welcome. However, note that **certain types of requests will
be rejected outright**:

1. **Requests which the author of this repo don't find as funny or
interesting.** If you don't like some of the replacements, the fork button is at
the top of the page. Note that removing a replacement in the plugin settings
will cause it to be recreated the next time an update is done - if this bothers
you, either turn off the auto update, or fork the repo.

2. **Requests which appear created to "add balance" or "remove offensiveness".**
These requests will not only be closed, but the author mocked for both not
reading the readme and for missing the entire point of such a list. This is a
personal list that I happen to find funny/relevant. If you don't like it, either
fork it or don't use it.

3. **Requests which remove items in general.** Spelling or content fixes are
fine and welcome, but in general, changes which remove items from the replace
list outright, barring a very good *technical* reason (inaccuracy, page
breakage, or something that makes sites too hard to read) will be denied.

Pull Request How-to
-------------------
Please create any additions using the tools provided by FoxReplace, rather than
hacking the JSON by hand. You can do so if you really want, but you stand a
greater chance of mucking up the JSON somehow.

Once you've made your changes in the FoxReplace options, you should:

1. Click on the export button, and save the resulting file.
2. Open up the file with your favorite text editor.
3. Select all the text, copy it.
4. [Use this link](https://github.com/Karunamon/npc-word-replacer/edit/master/FoxReplace.json),
delete all the text, and paste in what you just copied.

GitHub will walk you through the pull request process from there on.

License
-------
You can license a list of word replacements?

Well, just in case:

To the extent possible under law, I waive all copyright and related or
neighboring rights to NPC Word Replacer.

[![CC0 Logo](http://i.creativecommons.org/p/zero/1.0/88x31.png)](http://creativecommons.org/publicdomain/zero/1.0/)
